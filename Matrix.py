import random
POP_METHODS = ['Gaussian', 'zeros', 'identity']

class Matrix:
    def __init__(self, values=None, dims=None, population_method='Gaussian'):
        """[Initialize a Matrix object by either providing values or the dimensions and population method (optional) of the matrix]

        Args:
            values (2d list of numbers, optional): The element values. Required if dims not provided.
            dims (tuple with length 2 containing integers greater than 0, optional): Describes the dimensions of the matrix to create. Required if values not provided.
            population_method (str, optional): Population method for the matrix. Not used if values provided. Optional, defaults to 'Gaussian'. Accepts: 'Gaussian', 'zeros' and 'identity' (requires rows==cols)

        Raises:
            Exception: [description]
            Exception: [description]
            Exception: [description]
        """
        if values is None:
            if dims is None:
                raise Exception('Init error: missing argument dims')
            if population_method not in POP_METHODS:
                raise Exception('Init error: unknown population_method ' + str(population_method))
            self.__checkValidDims(dims)
                
            self.values = self.__populate(dims, population_method)
            self.dims = dims
        else:
            dims = self.__getDims(values)
            self.values = self.__checkValidValues(dims, values)
            self.dims = dims
        
        self.rows, self.cols = self.dims


    def copyValues(self):
        output = []
        for i in range(self.rows):
            row = []
            for j in range(self.cols):
                row.append(self.values[i][j])
            output.append(row)
        return output

    def __create_arr(self, dims, value=None, function=None):
        if value is None and function is None:
            raise Exception('Argument exception: missing value or function argument')
        rows, cols = dims
        values = []

        if value is not None:
            for i in range(rows):
                row = []
                for j in range(cols):
                    row.append(value)
                values.append(row)
        elif function is not None:
            for i in range(rows):
                row = []
                for j in range(cols):
                    row.append(function(i, j))
                values.append(row)
        return values

    def __populate(self, dims, population_method):
        if population_method == 'Gaussian':
            mu, sigma = 0, 1
            return self.__create_arr(dims, function=lambda row, col: random.gauss(mu, sigma))
        if population_method == 'zeros':
            return self.__create_arr(dims, value=0)
        if population_method == 'Identity':
            def identityfunc(row, col):
                if row == col:
                    return 1
                else: 
                    return 0
            return self.__create_arr(dims, function=identityfunc)
            
    def __checkValidDims(self, dims): # Check __init__ dims argument is valid
        if type(dims) is not tuple:
            raise Exception('Init error: dims argument not tuple')
        if len(dims) != 2:
            raise Exception('Init error: invalid dims argument. Expected length 2 but received length ' + len(dims))

    def __checkValidValues(self, dims, values):
        rows, cols = dims
        for i in range(rows):
            for j in range(cols):
                if type(values[i][j]) is not float and type(values[i][j]) is not int:
                    raise Exception('Init error: invalid values. Expected ints/floats only but (also) received ' + str(type(values[i][j])))
        return values

    def __getDims(self, array):
        def onlyLists(arr):
            for i in range(len(arr)):
                if type(arr[i]) is not list:
                    return False
            return True

        def someLists(arr):
            for i in range(len(arr)):
                if type(arr[i]) is list:
                    return True
            return False
            
        if type(array) is list:
            if onlyLists(array):
                # The array contains only lists as immediate children/elements
                prev_dims = None
                for i in range(len(array)):
                    dims = self.__getDims(array[i])
                    if prev_dims is None:
                        prev_dims = dims
                    else:
                        if prev_dims != dims:
                            raise Exception('Init error values contains lists of differing lengths.')
                # Every child has same dims, prepend own length and return
                prev_dims = (len(array),) + prev_dims
                return prev_dims
            elif someLists(array):
                raise Exception('Init error values contains both lists and primitives')
            else:
                # The array contains no lists, return current length
                return (len(array),)        
        else:
            return ()

    def __scalarMult(self, scalar):
        output = self.copyValues()
        for row in range(self.rows):
            for col in range(self.cols):
                output[row][col] *= scalar
        return Matrix(values=output)
    
    def __scalarAdd(self, scalar):
        output = self.copyValues()
        for row in range(self.rows):
            for col in range(self.cols):
                output[row][col] += scalar
        return Matrix(values=output)

    def __mul__(self, other):
        if type(other) is Matrix:
            output = self.__create_arr((self.rows, other.cols), value=0) 

            if self.cols != other.rows:
                raise Exception('Invalid matrix multiplication. Cannot multiply matrices with dims ' + str(self.dims) + ' and ' + str(other.dims))
            for i in range(0, self.rows):
                for j in range(0, other.cols):
                    elem_sum = 0
                    for k in range(0, self.cols):
                        elem_sum += self[i][k] * other[k][j]
                    output[i][j] = elem_sum
            return Matrix(output)
        elif type(other) is int or type(other) is float:
            return self.__scalarMult(other)

    def __rmul__(self, other):
        # other * self
        if type(other) is Matrix:
            return other.__mul__(self)
        elif type(other) is int or type(other) is float:
            return self.__scalarMult(other)
        else:
            raise Exception('Invalid matrix multiplication. Cannot multiply object of type ' + str(type(other)) + ' by Matrix object')

    def __add__(self, other):
        if type(other) is Matrix:
            if self.rows == other.rows and self.cols == other.cols:
                output = self.__create_arr(self.dims, value=0)
                for i in range(self.rows):
                    for j in range(self.cols):
                        output[i][j] = self[i, j] + other[i, j]
                return Matrix(values=output)
            else:
                raise Exception('Invalid matrix operation. Cannot add/subtract matricees with differing dimensions.')
        elif type(other) is int or type(other) is float:
            return self.__scalarAdd(other)
        else:
            raise Exception('Invalid matrix operation. Cannot add/subtract object of type ' + str(type(other)) + ' to/from Matrix object')

    def __radd__(self, other):
        # other + self = self + other
        return self.__add__(other)

    def __sub__(self, other):
        # self - other = self + (-1 * other)
        return self + (-1 * other)
        
    def __rsub__(self, other):
        # other - self = other + (-1 * self)
        return (-1 * self) + other

    def __getitem__(self, key):
        def throwKeyError():
            raise Exception('Index error: index ' + str(key) + ' is invalid for Matrix with dims ' + str(self.dims))
        
        if type(key) is tuple:
            if len(key) == 2:
                row, col = key
                if row > self.rows or row < 0:
                    throwKeyError() 
                if col > self.cols or col < 0:
                    throwKeyError()
                return self.values[row][col]
            else:
                throwKeyError()
        elif type(key) is int:
            if key > len(self.values) or key < 0:
                throwKeyError()
            return self.values[key]
        else:
            throwKeyError()

    def __str__(self):
        return str(self.values)

    def apply(self, function):
        output = self.copyValues()
        for i in range(self.rows):
            for j in range(self.cols):
                output[i][j] = function(i, j, output[i][j])
        return Matrix(values=output)

    def isRowVector(self):
        return self.rows == 1
    
    def isColVector(self):
        return self.cols == 1

    def isVector(self):
        return (self.cols == 1 and self.rows > 1) or (self.rows == 1 and self.cols > 1)

    def transpose(self):
        output = self.__create_arr((self.cols, self.rows), value=0)
        for i in range(self.cols):
            for j in range(self.rows):
                output[i][j] = self.values[j][i]
        return Matrix(values=output)

    def hadamard(self, other):
        if type(other) is not Matrix:
            raise Exception('Cannot perform Hadamard product of object of type ' + str(type(other)) + ' and Matrix object')
        if other.dims != self.dims:
            raise Exception('Cannot perform Hadamard product between Matrices with differing dimentions')

        output = self.__create_arr(self.dims, value=0)
        for i in range(self.rows):
            for j in range(self.cols):
                output[i][j] = self[i, j] * other[i, j]
        return Matrix(values=output)

    def getValuesInCol(self, col_idx):
        output = []
        for i in range(self.rows):
            output.append(self.values[i][col_idx])
        return output

    def getValuesInRow(self, row_idx):
        output = []
        for i in range(self.cols):
            output.append(self.values[row_idx][i])
        return output