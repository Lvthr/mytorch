from Matrix import Matrix
from MyTorch import Sigmoid, Quadratic
import math

class NN:
    def __init__(self, layer_sizes, activation_function=Sigmoid(), cost_function=Quadratic(), learningrate=0.01):
        self.__validateLayerSizes(layer_sizes)
        self.weights = [None]
        self.biases = []
        self.layers = []
        self.activations = []
        self.network_depth = len(layer_sizes)
        for i in range(len(layer_sizes)):
            if i < len(layer_sizes) - 1:
                weight_dims = (layer_sizes[i + 1], layer_sizes[i])
                weight = Matrix(dims=weight_dims)
                self.weights.append(weight)
            
            bias = Matrix(dims=(layer_sizes[i], 1))
            self.biases.append(bias)
            self.layers.append(Matrix(dims=(layer_sizes[i], 1), population_method='zeros'))
            self.activations.append(Matrix(dims=(layer_sizes[i], 1), population_method='zeros'))
        
        self.activation_function = activation_function
        self.cost_function = cost_function
        self.learningrate = learningrate
        

    def __validateLayerSizes(self, layer_sizes):
        if type(layer_sizes) is not list:
            raise Exception('Init error: layer_sizes argument is not of type list')
        
        for i in range(len(layer_sizes)):
            if type(layer_sizes[i]) is not int:
                raise Exception('Init error: layer_sizes element ' + str(i) + ' is not an integer')
            if layer_sizes[i] < 1:
                raise Exception('Init error: layer sizes element ' + str(i) + ' has invalid value (' + str(layer_sizes[i]) + '). Must be >= 1.')
        
    def forward(self, input):
        if type(input) is not Matrix:
            raise Exception('Input error: incorrect input type. Expected Matrix but received object of type ' + str(type(input)))
        if input.dims != self.layers[0].dims:
            raise Exception('Input error: incorrect input size. Expected Matrix with dims ' + str(self.layers[0].dims) + ' but received Matrix with dims ' + str(input.dims))
        
        for i in range(self.network_depth):
            if i == 0:
                # First layer, no activation
                self.layers[i] = input
                self.activations[i] = self.layers[i]
            elif i != self.network_depth - 1:
                self.layers[i] = (self.weights[i] * self.activations[i - 1]) + self.biases[i] # current layer equals weight between current layer and the one before multiplied by the previous layer. 
                self.activations[i] = self.activation_function.activate(self.layers[i]) # only perform activation for hidden neurons
            else:
                # Last layer, no activation
                self.layers[i] = (self.weights[i] * self.activations[i - 1]) + self.biases[i]
                self.activations[i] = self.activation_function.activate(self.layers[i])

        return self.activations[self.network_depth - 1]

    def backward(self, input, prediction, target):
        L = self.network_depth - 1 # constant value
        l = self.network_depth - 1 # initial value
        deltas = [None for i in range(self.network_depth)]
        while l > 0:
            if l == L:
                # Output layer 
                dCost = self.cost_function.derivative(target, self.activations[l]) # derivative of cost with respect to output activation/layer, depends on cost function implemented
                dAct = self.activation_function.derivative(self.layers[l])
                hadamard_product = dCost.hadamard(dAct)
                deltas[l] = hadamard_product
            else:
                delta = (self.weights[l + 1].transpose() * deltas[l + 1]).hadamard(self.activation_function.derivative(self.layers[l]))
                deltas[l] = delta
            l -= 1

        biasDeltas = deltas
        weightDeltas = []
        l = self.network_depth - 1
        while l > 0:
            rows, cols = self.weights[l].dims
            dWeight = []
            for j in range(rows):
                drow = []
                for k in range(cols):
                    drow.append(self.activations[l - 1][k][0] * deltas[l][j][0]) # TODO: Update indexing of Matrix class, such that if the matrix is a vector and __getitem__ receives only a single int key, returns the actual number
                dWeight.append(drow)
            weightDeltas.insert(0, Matrix(values=dWeight))
            l -= 1

        weightDeltas.insert(0, None)

        for i in range(1, self.network_depth):
            self.weights[i] = self.weights[i] - self.learningrate * weightDeltas[i]
            self.biases[i] = self.biases[i] - self.learningrate * biasDeltas[i]
        
    def cost(self, prediction, target):
        return self.cost_function.cost(target, prediction)

    def __str__(self):
        output = ''
        node_sym = '*'
        weight_sym = '|'
        spacer = ' '
        newline = '\n\n'
        max_size = None
        for i in range(self.network_depth):
            rows, cols = self.layers[i].dims
            if max_size is None:
                max_size = rows
            elif max_size < rows:
                max_size = rows
        

        for i in range(self.network_depth):
            line = ''
            rows, cols = self.layers[i].dims

            if rows < max_size:
                p = max_size - rows
                print(p)
                for i in range(p):
                    line += spacer

            for i in range(rows):
                line += node_sym + spacer
            output += (line + newline)
        return output

if __name__ == '__main__':
    layer_sizes = [4, 3, 2]
    nn = NN(layer_sizes, learningrate=0.5)

    input = Matrix(values=[[1, 0, 0.3, 0.2]]).transpose()
    target = Matrix(values=[[0, 1]]).transpose()

    MAX_ITTS = 200
    itteration = 0
    cost = 100
    cost_lim = 0.001
    while cost > cost_lim and itteration < MAX_ITTS:
        itteration += 1
        output = nn.forward(input)
        cost = nn.cost(target, output)
        nn.backward(input, output, target)
        print('Itteration:', itteration, ' current cost: ', cost, ' current output:', output, 'target:', target)


    print(nn)


