from Matrix import Matrix
import numpy as np
from abc import ABC, abstractclassmethod # Import Abstract Base Class(es) - ABC for creating ActivationFunc base class
import math

class ActivationFunc(ABC): # Define an abstract class, that has the required 
    def activation(self, matrix):
        pass

    def derivative(self, matrix):
        pass

    def raiseExep(self):
        raise Exception('Invalid input to activation function')

class Sigmoid(ActivationFunc):
    def __init__(self):
        pass

    def __sigmoidNumber(self, number):
        return 1 / (1 + math.exp(-number))

    def __dSigmoidNumber(self, number):
        return self.__sigmoidNumber(number) * (1 - self.__sigmoidNumber(number))


    def activate(self, value):
        if type(value) is Matrix:
            return value.apply(lambda i, j, val: self.__sigmoidNumber(val))
        elif type(value) is int or type(value) is float:
            return self.__sigmoidNumber(value)
        else:
            super().raiseExep()

    def derivative(self, value): # The derivative of the activation(function), with respect to the input
        if type(value) is Matrix:
            return value.apply(lambda i, j, val: self.__dSigmoidNumber(val))
        elif type(value) is int or type(value) is float:
            return self.__dSigmoidNumber(value)
        else:
            super().raiseExep()
        
class CostFunc(ABC):
    def checkInput(self, target, prediction):
        if type(target) is not Matrix or type(prediction) is not Matrix:
            raise Exception('Error, cannot compute cross entopy loss between objects that are not matrices'+ str(type(target)) + ' and ' + str(type(prediction)))
    
        if prediction.dims != target.dims:
            raise Exception('Error, prediction and target has differing dims: ' + str(target.dims) + ' and ' + str(prediction.dims))

        t, p = None, None
        if target.isRowVector():
            t, p = target.getValuesInRow(0), prediction.getValuesInRow(0)
        elif target.isColVector():
            t, p = target.getValuesInRow(0), prediction.getValuesInRow(0)
        else:
            raise Exception('Input target and/or prediction are neither row nor column vectors. Cannot compute cost')

        return t, p
        
    def cost(self, target, prediction):
        pass

    def derivative(self, target, prediction): # the derivative of the cost function, with respect to the prediction
        pass

class Quadratic(CostFunc):
    def cost(self, target, prediction):
        t, p = super().checkInput(target, prediction)
        summed = 0
        for i in range(len(t)):
            summed += (t[i] - p[i])**2
        return summed / 2

    def derivative(self, target, prediction):
        super().checkInput(target, prediction)
        return prediction - target
